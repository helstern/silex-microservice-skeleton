<?php
/** @var ClassLoader $classLoader */
use Composer\Autoload\ClassLoader;
$classLoader = require __DIR__ . '/../../app/vendor/autoload.php';
$classLoader->addPsr4('', __DIR__);

Definition::declareAnnotationConstants();
