<?php

use Helstern\SMSkeleton\Manifest;
use Swagger\Annotations as SWG;

/**
 * @SWG\Swagger(
 *     basePath=API_BASE_PATH,
 *     consumes={"application/json"},
 *     host="localhost",
 *     produces={"application/json"},
 *     schemes={"http"},
 *     responses={
 *      @SWG\Response(
 *          response=400,
 *          description="syntactic error",
 *          @SWG\Schema(ref="#/definitions/ErrorResponse")
 *      )
 *     }
 * )
 *
 * @SWG\Info(
 *   title="silex microservice skeleton",
 *   description="a sample silex microservice skeleton",
 *   version=API_VERSION,
 *   termsOfService="http://localhost/terms/",
 *   @SWG\Contact(
 *     email="radu.helstern+silex-microservice-skeleton@helstern.tech",
 *     name="Radu Helstern",
 *     url="http://localhost"
 *   ),
 *   @SWG\License(
 *     name="Apache 2.0",
 *     url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *   ),
 *   termsOfService="http://swagger.io/terms/"
 * )
 */
class Definition
{
    public static function declareAnnotationConstants()
    {
        define('API_BASE_PATH', Manifest::API_BASE_PATH);
        define('API_VERSION', Manifest::API_VERSION);
    }
}
