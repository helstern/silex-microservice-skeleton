#!/bin/bash

# wait for other containers to expose their ports
while ! nc -z "${DOCKER_PROJECT_NAME}-mysql" 3306; do sleep 3; done
echo 'connections with other containers ready'

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USERNAME=www-data
USER_ID=${LOCAL_USER_ID:-9001}

echo "Starting with UID : ${USER_ID}"
if [ "${USER_ID}" != "$(id -u ${USERNAME})" ]; then
    usermod -u ${USER_ID} ${USERNAME}
fi

cat < /run/php/access.log > /proc/self/fd/2
cat < /run/php/error.log > /proc/self/fd/2

if ! test -z "$@" ; then
    echo "Running: $@"
    exec /usr/local/bin/gosu ${USERNAME} $@
fi
