FROM ubuntu:16.04

ENV DOCKER_PROJECT_NAME=${DOCKER_PROJECT_NAME}
# Fixes some weird terminal issues such as broken clear / CTRL+L
ENV TERM=linux
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# runtime dependencies
RUN apt-get update && apt-get -y --no-install-recommends install \
    ca-certificates \
    curl \
    libedit2 \
    libsqlite3-0 \
    libsodium18 \
    libxml2 \
    libcurl3 \
    libpcre3 \
    libzip4 \
    libxslt1.1 \
    netcat-openbsd \
    sudo \
    xz-utils \
    && rm -r /var/lib/apt/lists/*

# Install gosu
RUN gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4
RUN curl -o /usr/local/bin/gosu -SL "https://github.com/tianon/gosu/releases/download/1.4/gosu-$(dpkg --print-architecture)" \
    && curl -o /usr/local/bin/gosu.asc -SL "https://github.com/tianon/gosu/releases/download/1.4/gosu-$(dpkg --print-architecture).asc" \
    && gpg --verify /usr/local/bin/gosu.asc \
    && rm /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu

# Install Ondrej repos for Ubuntu Xenial, PHP7.1, composer and selected extensions
RUN echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu xenial main" > /etc/apt/sources.list.d/ondrej-php.list \
    && echo "deb http://ppa.launchpad.net/ondrej/php-qa/ubuntu xenial main" > /etc/apt/sources.list.d/ondrej-php-qa.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
        php7.1-common \
        php-apcu \
        php-apcu-bc \
        php-libsodium \
        php7.1-cli \
        php7.1-curl \
        php7.1-fpm \
        php7.1-json \
        php7.1-mbstring \
        php7.1-mcrypt \
        php7.1-mysql \
        php7.1-opcache \
        php7.1-readline \
        php7.1-xml \
        php7.1-zip \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* ~/.composer

# Create named pipes for logging to standard streams when not running as root
RUN mkdir /run/php && chown www-data /run/php \
    && mkfifo --mode 0600 /run/php/access.log && chown www-data /run/php/access.log \
    && mkfifo --mode 0600 /run/php/error.log && chown www-data /run/php/error.log

# Configure FPM to run properly on docker
COPY php-fpm.conf /etc/php/7.1/fpm/php-fpm.conf
COPY www-10.conf /etc/php/7.1/fpm/pool.d/www-10.conf

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
# The following runs FPM and removes all its extraneous log output on top of what your app outputs to stdout
CMD [ "/usr/sbin/php-fpm7.1 -F -O 2>&1 | sed -u 's,.*: \"\\(.*\\)$,\\1,'| sed -u 's,\"$,,' 1>&1" ]

# Open up fast cgi port
EXPOSE 9000

WORKDIR "/var/www/service"
