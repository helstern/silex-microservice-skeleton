#!/bin/bash

# wait for other containers to expose their ports
while ! nc -z ${DOCKER_PROJECT_NAME}-mysql 3306; do sleep 3; done
echo 'connections with other containers ready'

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

USERNAME=test
USER_ID=${LOCAL_USER_ID:-9001}


if [ "${USER_ID}" != "$(id -u ${USERNAME})" ]; then
    echo "Starting with UID : ${USER_ID}"
    usermod -u ${USER_ID} ${USERNAME}
fi

if test "$#" -ne "0" ; then
    echo "executing $@"
    exec /usr/local/bin/gosu ${USERNAME} "$@"
fi
