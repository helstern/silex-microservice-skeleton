<?php namespace Helstern\SMSkeleton\HttpApi\Monitoring;

use Silex;
use Symfony\Component\HttpFoundation\Request;

class HealthCheckController
{
    /**
     * @SWG\Get(
     *     path="/monitoring/healthcheck",
     *     operationId="monitoring/healthcheck",
     *     description="Returns information about the health of the api",
     *     @SWG\Response(
     *         response=200,
     *         description="greeting response",
     *         @SWG\Schema(ref="#/definitions/HealthCheck")
     *     )s
     * )
     */
    public function get(Request $request, Silex\Application $app)
    {
        return new HealthCheck('healthy');
    }
}
