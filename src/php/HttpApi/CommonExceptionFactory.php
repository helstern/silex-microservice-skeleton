<?php namespace Helstern\SMSkeleton\HttpApi;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class CommonExceptionFactory
{
    /**
     * @param \Exception $previous
     * @param ConstraintViolationListInterface $violationList
     * @return Exception
     */
    public function createValidationException(\Exception $previous, ConstraintViolationListInterface $violationList)
    {
        $errorList = [];
        foreach ($violationList as $violation) {
            $errorList[] = $this->mapConstraintViolationToApiValidationError($violation, []);
        }

        $message = 'validation error';

        $responseEntity = new ErrorResponse();
        $responseEntity->setMessage($message);
        $responseEntity->setDetails($errorList);

        $exception = new Exception(Response::HTTP_BAD_REQUEST, [], $responseEntity, $message, $previous);
        return $exception;
    }

    private function mapConstraintViolationToApiValidationError(ConstraintViolationInterface $violation, array $errorEntity)
    {
        $errorEntity['message'] = $violation->getMessage();
        $errorEntity['property'] = $violation->getPropertyPath();
        return $errorEntity;
    }
}
