<?php namespace Helstern\SMSkeleton\HttpApi;

use Helstern\SMSkeleton\Infrastructure\ErrorHandling;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use JMS\Serializer;

class ExceptionListener implements EventSubscriberInterface
{
    /** @var Serializer\Serializer */
    private $serializer;

    public function __construct(Serializer\Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Logs uncaught exceptions on event KernelEvents::EXCEPTION.
     *
     * @param GetResponseForExceptionEvent $event
     * @return HttpFoundation\JsonResponse
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $request = $event->getRequest();

        if ($exception instanceof Exception) {
            $response = $this->createErrorResponse($exception, $request);
        } else {
            $response = $this->createServerErrorResponse($exception, $request);
        }

        $event->setResponse($response);
        return $response;
    }

    private function createErrorResponse(Exception $exception, HttpFoundation\Request $request)
    {
        $data = $this->serializer->serialize($exception->getResponseEntity(), 'json');
        return new HttpFoundation\JsonResponse($data, $exception->getStatusCode(), [], true);
    }

    private function createServerErrorResponse(\Exception $exception, HttpFoundation\Request $request)
    {
        $response = new ErrorResponse();
        $response->setMessage('unexpected server error');

        $data = $this->serializer->serialize($response, 'json');
        return new HttpFoundation\JsonResponse($data, HttpFoundation\Response::HTTP_INTERNAL_SERVER_ERROR, [], true);
    }

    public static function getSubscribedEvents()
    {
        return [
            /*
             * Priority -4 is used to come after those from SecurityServiceProvider (0)
             * but before the error handlers added with Silex\Application::error (defaults to -8)
             */
            KernelEvents::EXCEPTION => ['onKernelException', ErrorHandling\ErrorHandlerPriority::AFTER_ERROR_REPORTER]
        ];
    }
}
