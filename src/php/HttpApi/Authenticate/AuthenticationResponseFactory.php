<?php namespace Helstern\SMSkeleton\HttpApi\Authenticate;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationResponseFactory
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createAuthenticationFailureResponse(Request $request)
    {
        $data = ['message' => 'Authentication Required'];
        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function createAuthenticationStartResponse(Request $request)
    {
        $data = ['message' => 'Authentication Required'];
        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}
