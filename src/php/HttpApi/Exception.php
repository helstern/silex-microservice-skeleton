<?php namespace Helstern\SMSkeleton\HttpApi;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class Exception
 * @package HttpApi
 * @ExclusionPolicy¶
 */
class Exception extends HttpException
{
    /** @var ErrorResponse */
    private $entity;

    /**
     * Exception constructor.
     * @param string $statusCode
     * @param array $headers
     * @param mixed $entity
     * @param array $message
     * @param \Exception $previous
     * @param int $code
     */
    public function __construct($statusCode, array $headers, ErrorResponse $entity, $message, \Exception $previous, $code = 0)
    {
        parent::__construct($statusCode, $message, $previous, $headers, $code = 0);
        $this->entity = $entity;
    }

    /**
     * @return object|object[]|null
     */
    public function getResponseEntity()
    {
        return $this->entity;
    }
}
