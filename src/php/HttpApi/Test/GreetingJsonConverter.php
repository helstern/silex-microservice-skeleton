<?php namespace Helstern\SMSkeleton\HttpApi\Test;

use Helstern\SMSkeleton\HttpApi\Exception;
use Helstern\SMSkeleton\HttpApi\CommonExceptionFactory;
use JMS\Serializer;
use Symfony\Component\HttpFoundation\Request;

class GreetingJsonConverter
{
    /** @var Serializer\Serializer */
    private $serializer;

    public function __construct(Serializer\Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $options
     * @param Request $request
     * @return GreetingOptions
     * @throws Exception
     */
    public function convertToOptionsObject($options, Request $request)
    {
        /** @var GreetingOptions $details */
        $converted = null;
        try {
            $jsonData = $request->getContent();
            $converted = $this->serializer->deserialize($jsonData, GreetingOptions::class, 'json');
            return $converted;
        } catch (Serializer\Exception\ValidationFailedException $e) { //syntax failure
            $exceptionFactory = new CommonExceptionFactory();
            $apiException = $exceptionFactory->createValidationException($e, $e->getConstraintViolationList());
            throw $apiException;
        }
    }

    /**
     * @param $greeting
     * @param Request $request
     * @return Greeting
     * @throws Exception
     */
    public function convertToObject($greeting, Request $request)
    {
        /** @var Greeting $details */
        $converted = null;
        try {
            $jsonData = $request->getContent();
            $converted = $this->serializer->deserialize($jsonData, Greeting::class, 'json');
            return $converted;
        } catch (Serializer\Exception\ValidationFailedException $e) { //syntax failure
            $exceptionFactory = new CommonExceptionFactory();
            $apiException = $exceptionFactory->createValidationException($e, $e->getConstraintViolationList());
            throw $apiException;
        }
    }

    /**
     * @param Greeting $greeting
     * @param Request $request
     * @return string
     */
    public function convertToJson(Greeting $greeting, Request $request)
    {
        return $this->serializer->serialize($greeting, 'json');
    }
}
