<?php namespace Helstern\SMSkeleton\HttpApi\Test;

use JMS\Serializer\Annotation;
use Symfony\Component\Validator\Constraints;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *   definition="GreetingOptions",
 *   type="object",
 *   required={"recipient"}
 * )
 */
class GreetingOptions
{
    /**
     * @SWG\Property(type="string")
     * @Annotation\Type("string")
     * @var string
     */
    private $salutation;

    /**
     * @SWG\Property(type="string")
     * @Annotation\Type("string")
     * @Constraints\NotBlank()
     * @Constraints\NotNull()
     * @var string
     */
    private $recipient;

    /**
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * @param string $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param string $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }
}
