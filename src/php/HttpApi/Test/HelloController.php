<?php namespace Helstern\SMSkeleton\HttpApi\Test;

use Silex;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;

class HelloController
{
    /**
     * @SWG\Post(
     *     path="/test/hello",
     *     operationId="test/hello",
     *     description="Creates a new hello greeting",
     *     @SWG\Parameter(
     *         name="options",
     *         in="body",
     *         description="the options to construct the greeting with",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/GreetingOptions"),
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="greeting response",
     *         @SWG\Schema(ref="#/definitions/Greeting")
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         ref="#/responses/400"
     *     )
     * )
     */
    public function post(Request $request, Silex\Application $app, GreetingOptions $options)
    {
        $greeting = new Greeting();
        $greeting->setSalutation('Hello');
        $greeting->setRecipient($options->getRecipient());
        return $greeting;
    }
}
