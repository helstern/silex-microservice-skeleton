<?php namespace Helstern\SMSkeleton\ConsoleApi\Authenticate;

use Helstern\SMSkeleton\Application\SecureUserTokenIssuer;
use Helstern\SMSkeleton\Infrastructure\Application;
use Helstern\SMSkeleton\Infrastructure\Console\ConsoleCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ApiTokenCommand extends ConsoleCommand
{
    /**
     * TokenCommand constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDefinition([
                new InputArgument('username', InputArgument::REQUIRED, 'The username used by the token')
            ])
            ->setDescription('Generates a token used to access the api on behalf of a user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::validateOptions($input);

        $username = $input->getArgument('username');
        $issuer = $this->getSilexApplication()->offsetGet(SecureUserTokenIssuer::class);
        $token = $issuer->issueForUser($username);

        $output->writeln($token);
    }
}
