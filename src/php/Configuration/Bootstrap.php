<?php namespace Helstern\SMSkeleton\Configuration;

use Helstern\SMSkeleton\HttpApi;
use Helstern\SMSkeleton\Configuration\Provider;
use Helstern\SMSkeleton\Infrastructure\Validation;
use Helstern\SMSkeleton\Infrastructure\ErrorHandling;
use Helstern\SMSkeleton\Infrastructure\Application;
use Helstern\SMSkeleton\Manifest;
use Silex;

class Bootstrap
{
    public function bootstrap(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $this->bootstrapEnv($app, $env, $now);

        //db persistence
        $app->register(new Provider\PersistenceMysql($env));

        $this->bootstrapHttpApi($app, $env, $now);

        $this->bootstrapSecurity($app, $env, $now);

        $app->register(new Provider\ErrorHandling());

        //validation
        $app->register(
            new Silex\Provider\ValidatorServiceProvider(), ['validator.mapping.class_metadata_factory' => Validation::createAnnotationClassMetadataFactory()]
        );

        //cache
        $cacheDir = $app->getCacheDir();
        $app->register(new Silex\Provider\HttpCacheServiceProvider(), ['http_cache.cache_dir' => $cacheDir]);

        //logging
        $this->bootstrapApplicationLogging($app, $env, $now);
    }

    private function bootstrapEnv(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $mapping = ['debug' => Env::DEBUG_MODE];
        $this->mapEnvironmentValues($env, $mapping, $app);
    }

    private function mapEnvironmentValues(array $env, array $mapping, \ArrayAccess $mapInto)
    {
        $values = [];
        foreach ($mapping as $key => $envKey) {
            if (is_callable($envKey)) {
                $values[$key] = $envKey($env);
            } elseif (array_key_exists($envKey, $env)) {
                $values[$key] = $env[$envKey];
            }
        }

        foreach ($values as $key => $value) {
            $mapInto->offsetSet($key, $value);
        }
    }

    private function bootstrapApplicationLogging(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        //infrastructure logging
        $logsDir = $app->getLogsDir();
        $monologConfiguration = [
            'monolog.use_error_handler' => false,
            'monolog.listener' => null,
            'monolog.logfile' => sprintf($logsDir.'/%s.log', $now->format('Y-m-d')),
            'monolog.level' => $env[Env::LOG_SEVERITY],
            'monolog.name' => 'application',
        ];
        $app->register(new Silex\Provider\MonologServiceProvider(), $monologConfiguration);
        $app->offsetUnset('monolog.listener');
    }

    private function bootstrapHttpApi(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $appProvider = new Provider\HttpApi();
        $app->mount(Manifest::API_BASE_PATH, $appProvider);

        //allow service:method syntax when defining controllers
        $app->register(new Silex\Provider\ServiceControllerServiceProvider());
        $app->register($appProvider);

        //serialization
        $app->register(new Provider\SerializerJson());
    }

    private function bootstrapSecurity(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $mapping = [
            'secret' => function (array $env) {
                return base64_decode($env[Env::JWT_SECRET]);
            }
            , 'algorithm' => Env::JWT_ALGORITHM
        ];
        $this->mapEnvironmentValues($env, $mapping, $app);

        $app['serverName'] = 'service';
        $app->register(new Silex\Provider\SecurityServiceProvider());
        $app->register(new Provider\Security());
    }
}
