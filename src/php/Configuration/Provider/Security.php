<?php namespace Helstern\SMSkeleton\Configuration\Provider;

use Helstern\SMSkeleton\Manifest;
use Helstern\SMSkeleton\Application\SecureUserTokenIssuer;
use Helstern\SMSkeleton\Configuration;
use Helstern\SMSkeleton\HttpApi\Authenticate;
use Helstern\SMSkeleton\Infrastructure;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Application;

class Security implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app[SecureUserTokenIssuer::class] = function ($app) {
            $tokenIssuer = new Infrastructure\Security\JWTTokenIssuer($app['serverName'], $app['secret'], $app['algorithm']);
            return $tokenIssuer;
        };

        $app['security.firewalls'] = Security::createFirewallConfiguration($app);
        $app['api_client_authenticator'] = function (Container $app) {
            $authcResponseFactory = new Authenticate\AuthenticationResponseFactory();
            return new Infrastructure\Security\BasicAuthAuthenticator($authcResponseFactory);
        };
        $app['api_token_authenticator'] = function (Container $app) {
            $tokenIssuer = $app->offsetGet(SecureUserTokenIssuer::class);
            $authcResponseFactory = new Authenticate\AuthenticationResponseFactory();
            return new Infrastructure\Security\TokenAuthenticator($tokenIssuer, $authcResponseFactory);
        };
    }

    /**
     * @param Container $app
     * @return array
     */
    public static function createFirewallConfiguration(Container $app)
    {
        return [
            '/test/hello' => [
                'anonymous' => false,
                'pattern' => sprintf('^%s.*$', Manifest::API_BASE_PATH . '/test/hello'),
                'guard' => [
                    'authenticators' => [
                        'api_token_authenticator'
                    ]
                ],
                'stateless' => true
            ],
            '/monitoring/healthcheck' => [
                'anonymous' => true,
                'pattern' => sprintf('^%s.*$', Manifest::API_BASE_PATH . '/monitoring/healthcheck'),
                'guard' => [
                    'authenticators' => [
                        'api_token_authenticator'
                    ]
                ],
                'stateless' => true
            ]
        ];
    }
}
