<?php namespace Helstern\SMSkeleton\Configuration\Provider;

use Helstern\SMSkeleton\Application;
use Helstern\SMSkeleton\HttpApi\ExceptionListener;
use Helstern\SMSkeleton\HttpApi\Monitoring;
use Helstern\SMSkeleton\HttpApi\Test;
use Helstern\SMSkeleton\HttpApi\Authenticate;
use Helstern\SMSkeleton\Infrastructure\ErrorHandling;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Api\ControllerProviderInterface;
use Silex\Api\EventListenerProviderInterface;
use Silex\Application as SilexApplication;
use Silex\ControllerCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation;

class HttpApi implements ServiceProviderInterface, ControllerProviderInterface, EventListenerProviderInterface
{
    public function register(Container $app)
    {
        $this->registerMonitoringApi($app);
        $this->registerTestApi($app);
    }

    /**
     * Returns routes to connect to the given application.
     *
     * @param SilexApplication $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(SilexApplication $app)
    {
        /** @var ControllerCollection $controllers */
        $controllers = $app['controllers_factory']; // creates a new controller based on the default route
        $this->connectMonitoringApi($app, $controllers);
        $this->connectTestApi($app, $controllers);
        $this->registerRequestContentTypeHandler($app);

        return $controllers;
    }

    public function subscribe(Container $app, EventDispatcherInterface $dispatcher)
    {
        $serializer = $app["serializer.json"];
        $subscriber = new ExceptionListener($serializer);
        $dispatcher->addSubscriber($subscriber);
    }

    private function registerRequestContentTypeHandler(SilexApplication $app)
    {
        $app->before(function (HttpFoundation\Request $request) {
            if (0 !== strpos($request->headers->get('Content-Type'), 'application/json')) {
                return new HttpFoundation\JsonResponse(
                    ['message' => 'unsupported media type'], HttpFoundation\Response::HTTP_UNSUPPORTED_MEDIA_TYPE
                );
            }

            return null;
        });
    }

    private function connectMonitoringApi(SilexApplication $app, ControllerCollection $controllers)
    {
        $healthcheckRenderer = implode(':', [Monitoring\HealthCheckJsonConverter::class, 'convertToJson']);
        $app->view($healthcheckRenderer);

        $serviceMethod = implode(':', [Monitoring\HealthCheckController::class, 'get']);
        $controllers
            ->get('/monitoring/healthcheck', $serviceMethod)
            ->convert('receipt', 'payment.converter:convertToObject')
        ;
    }

    private function registerMonitoringApi(Container $app)
    {
        $app[Monitoring\HealthCheckController::class] = function (Container $app) {
            return new Monitoring\HealthCheckController($app);
        };

        $app[Monitoring\HealthCheckJsonConverter::class] = function (SilexApplication $app) {
            /** @var \JMS\Serializer\Serializer $serializer */
            $serializer = $app["serializer.json"];
            return new Monitoring\HealthCheckJsonConverter($serializer);
        };
    }

    private function connectTestApi(SilexApplication $app, ControllerCollection $controllers)
    {
        $greetingRenderer = implode(':', [Test\GreetingJsonConverter::class, 'convertToJson']);
        $app->view($greetingRenderer);

        $postHandler = implode(':', [Test\HelloController::class, 'post']);
        $greetingOptionsConverter = implode(':', [Test\GreetingJsonConverter::class, 'convertToOptionsObject']);
        $controllers
            ->post('/test/hello', $postHandler)
            ->convert('options', $greetingOptionsConverter)
            ->after(function (HttpFoundation\Request $request, HttpFoundation\Response $response) {
                if ($response->getStatusCode() == HttpFoundation\Response::HTTP_OK) {
                    $response->setStatusCode(HttpFoundation\Response::HTTP_CREATED);
                }
            })
        ;
    }

    private function registerTestApi(Container $app)
    {
        $app[Test\HelloController::class] = function (Container $app) {
            return new Test\HelloController($app);
        };

        $app[Test\GreetingJsonConverter::class] = function (SilexApplication $app) {
            /** @var \JMS\Serializer\Serializer $serializer */
            $serializer = $app["serializer.json"];
            return new Test\GreetingJsonConverter($serializer);
        };
    }
}
