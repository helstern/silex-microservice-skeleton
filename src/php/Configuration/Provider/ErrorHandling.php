<?php namespace Helstern\SMSkeleton\Configuration\Provider;

use Helstern\SMSkeleton\Infrastructure\ErrorHandling\PsrExceptionReporter;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Psr\Log\LoggerInterface;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class ErrorHandling implements ServiceProviderInterface, BootableProviderInterface
{
    public function register(Container $app)
    {
        $app->offsetSet('monolog.use_error_handler', true);
        $app['exception_reporter'] = function (Container $app) {
            /** @var LoggerInterface $logger */
            $logger = $app['logger'];
            return new PsrExceptionReporter($logger);
        };
    }

    public function boot(Application $app)
    {
        $app['dispatcher']->addSubscriber($app['exception_reporter']);
    }
}
