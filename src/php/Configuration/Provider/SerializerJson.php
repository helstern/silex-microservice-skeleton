<?php namespace Helstern\SMSkeleton\Configuration\Provider;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Helstern\SMSkeleton\Infrastructure\Serialization\JmsJsonSerializerBuilder;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use ReflectionClass;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

class SerializerJson implements ServiceProviderInterface, BootableProviderInterface
{
    public function boot(Application $app)
    {
        if ($app->offsetExists("serializer.srcDir")) {
            $reflector = new ReflectionClass(\JMS\Serializer\Serializer::class);
            $srcDir = dirname($reflector->getFileName(), count((explode('\\', \JMS\Serializer\Serializer::class))));

            AnnotationRegistry::registerAutoloadNamespace("JMS\\Serializer\\Annotation", $srcDir);
        }
    }

    public function register(Container $app)
    {
        $app["serializer.json.builder"] = new JmsJsonSerializerBuilder();
        $app["serializer.json"] = function (Container $app) {
            return $app["serializer.json.builder"]->build();
        };
    }
}
