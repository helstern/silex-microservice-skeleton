<?php namespace Helstern\SMSkeleton\Configuration\Provider;

use Helstern\SMSkeleton\Application;
use Helstern\SMSkeleton\Configuration;
use Doctrine\DBAL\Connection;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Provider;

class PersistenceMysql implements ServiceProviderInterface
{
    /** @var array */
    private $env;

    public function __construct(array $env)
    {
        $this->env = $env;
    }

    private function setDBOptionsFromEnv(array $dbOptions)
    {
        $varMapping = [
            'host' => Configuration\Env::DB_HOST,
            'port' => Configuration\Env::DB_PORT,
            'dbname' => Configuration\Env::DB_NAME,
            'user' => Configuration\Env::DB_USER,
            'password' => Configuration\Env::DB_PASSWORD,
        ];
        foreach ($varMapping as $confVar => $envVar) {
            $dbOptions[$confVar] = $this->env[$envVar];
        }

        return $dbOptions;
    }

    public function register(Container $app)
    {
        $app['db.options'] = $this->setDBOptionsFromEnv(['driver' => 'pdo_mysql']);

        $app->register(new Provider\DoctrineServiceProvider());
    }
}
