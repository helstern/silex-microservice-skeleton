<?php namespace Helstern\SMSkeleton\Configuration;

use Helstern\SMSkeleton\Manifest;

class Env
{
    const PREFIX = Manifest::IDENTIFIER . '_';

    const DB_HOST = Env::PREFIX . 'DB_HOST';

    const DB_PORT = Env::PREFIX . 'DB_PORT';

    const DB_NAME = Env::PREFIX . 'DB_NAME';

    const DB_USER = Env::PREFIX . 'DB_USER';

    const DB_PASSWORD = Env::PREFIX . 'DB_PASSWORD';

    const DEBUG_MODE = Env::PREFIX . 'DEBUG_MODE';

    const JWT_SECRET = Env::PREFIX . 'JWT_SECRET';

    const JWT_ALGORITHM = Env::PREFIX . 'JWT_ALGORITHM';

    const LOG_SEVERITY = Env::PREFIX . 'LOG_SEVERITY';
}
