<?php namespace Helstern\SMSkeleton\Configuration;

use Helstern\SMSkeleton\ConsoleApi\Authenticate\ApiTokenCommand;
use Helstern\SMSkeleton\Infrastructure\Console;
use Helstern\SMSkeleton\Infrastructure\Application;
use Silex\Provider;

class ConsoleBootstrap
{
    public function bootstrap(Application\SilexApplication $app, array $env, \DateTime $now)
    {
        $app['console'] = function () use ($app) {
            $console = new Console\ConsoleApplication($app, 'Service Cli', 'n/a');
            $console->setDispatcher($app['dispatcher']);
            $console->add(new ApiTokenCommand('api-token'));

            return $console;
        };
    }
}
