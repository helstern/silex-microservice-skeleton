<?php namespace Helstern\SMSkeleton\Infrastructure\Serialization;

use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\Naming\CamelCaseNamingStrategy;
use JMS\Serializer\SerializerBuilder;
use Pimple\Container;

class JmsJsonSerializerBuilder
{
    public function __invoke(Container $app)
    {
        $serializerBuilder = SerializerBuilder::create();

        if ($app->offsetExists("debug")) {
            $serializerBuilder->setDebug($app["debug"]);
        }

        if ($app->offsetExists("serializer.cacheDir")) {
            $serializerBuilder->setCacheDir($app["serializer.cacheDir"]);
        }

        $validator = $app['validator'];
        $serializerBuilder->configureListeners(function (EventDispatcher $eventDispatcher) use ($validator) {
            $validatorEventSubscriber = new SymfonyValidatorSubscriber($validator);
            $eventDispatcher->addSubscriber($validatorEventSubscriber);
        });

        $this->setPropertyNamingStrategy($app, $serializerBuilder);

        return $serializerBuilder;
    }

    protected function setPropertyNamingStrategy(Container $app, SerializerBuilder $serializerBuilder)
    {
        $serializationStrategy = new CamelCaseNamingStrategy($separator = '_', $lowerCase = true);
        $serializerBuilder->setPropertyNamingStrategy($serializationStrategy);

        return $serializationStrategy;
    }

    /**
     * Override default serialization vistors
     *
     * @param Container $app
     * @param SerializerBuilder $serializerBuilder
     */
    protected function setSerializationVisitors(Container $app, SerializerBuilder $serializerBuilder)
    {
        $serializerBuilder->addDefaultSerializationVisitors();

        foreach ($app["serializer.serializationVisitors"] as $format => $visitor) {
            $serializerBuilder->setSerializationVisitor($format, $visitor);
        }
    }

    /**
     * Override default deserialization visitors
     *
     * @param Container $app
     * @param SerializerBuilder $serializerBuilder
     */
    protected function setDeserializationVisitors(Container $app, SerializerBuilder $serializerBuilder)
    {
        $serializerBuilder->addDefaultDeserializationVisitors();

        foreach ($app["serializer.deserializationVisitors"] as $format => $visitor) {
            $serializerBuilder->setDeserializationVisitor($format, $visitor);
        }
    }
}
