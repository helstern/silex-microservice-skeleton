<?php namespace Helstern\SMSkeleton\Infrastructure\ErrorHandling;

class ErrorHandlerPriority
{
    const ERROR_REPORTER = -4;

    const AFTER_ERROR_REPORTER = -6;
}
