<?php namespace Helstern\SMSkeleton\Infrastructure\ErrorHandling;

use Helstern\SMSkeleton\Application;
use Helstern\SMSkeleton\HttpApi;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class PsrExceptionReporter implements EventSubscriberInterface
{
    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Logs uncaught exceptions on event KernelEvents::EXCEPTION.
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $request = $event->getRequest();
        $this->reportException($exception, $request);
    }

    public function reportException(\Exception $e, Request $request)
    {
        if ($e instanceof Application\Exception) {
            $this->reportApplicationException($e, $request);
            return;
        }

        $applicationExceptions = $this->collectApplicationExceptions($e);
        foreach ($applicationExceptions as $applicationException) {
            $this->reportApplicationException($applicationException, $request);
        }

        if ($e instanceof HttpApi\Exception) {
            $this->reportHttpApiException($e, $request);
            return;
        }

        $this->reportError($e, $request);
    }

    /**
     * @param \Exception $e
     * @return Application\Exception[]
     */
    private function collectApplicationExceptions(\Exception $e)
    {
        $collected = [];
        $previous = $e->getPrevious();
        while (! is_null($previous)) {
            if ($previous instanceof Application\Exception) {
                $collected[] = $previous;
            }
            $previous = $previous->getPrevious();
        }

        return array_reverse($collected);
    }

    private function reportApplicationException(Application\Exception $e, Request $request)
    {
        if ($e instanceof Application\Loggable) {
            $logLevel = $e->getSeverityLevel();
        } else {
            $logLevel = LogLevel::DEBUG;
        }

        $this->logger->log($logLevel, $e->getMessage(), ['trace' => $e->getTraceAsString()]);
    }

    private function reportHttpApiException(HttpApi\Exception $e, Request $request)
    {
        $this->logger->debug($e->getMessage(), ['trace' => $e->getTraceAsString()]);
    }

    public function reportError(\Exception $e, Request $request)
    {
        $this->logger->error($e->getMessage(), ['type' => get_class($e), 'trace' => $e->getTraceAsString()]);
    }

    public static function getSubscribedEvents()
    {
        return [
            /*
             * Priority -4 is used to come after those from SecurityServiceProvider (0)
             * but before the error handlers added with Silex\Application::error (defaults to -8)
             */
            KernelEvents::EXCEPTION => ['onKernelException', ErrorHandlerPriority::ERROR_REPORTER]
        ];
    }
}
