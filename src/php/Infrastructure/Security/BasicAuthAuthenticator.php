<?php namespace Helstern\SMSkeleton\Infrastructure\Security;

use Helstern\SMSkeleton\HttpApi\Authenticate;
use Symfony\Component\HttpFoundation;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class BasicAuthAuthenticator extends AbstractGuardAuthenticator
{
    /** @var Authenticate\AuthenticationResponseFactory */
    private $responseFactory;

    public function __construct(Authenticate\AuthenticationResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     */
    public function getCredentials(HttpFoundation\Request $request)
    {
        if (!$token = $request->headers->get('AUTHORIZATION')) {
            // no token? Return null and no other methods will be called
            return null;
        }
        $tokenParts = explode(' ', $token);
        if (count($tokenParts) == 2 && $tokenParts[0] == 'Basic') {
            try {
                $decoded = base64_decode($tokenParts[1]);
                $usernameAndPassword = explode(':', $decoded);
                if (count($usernameAndPassword) == 2) {
                    return [
                        'username' => $usernameAndPassword[0],
                        'password' => $usernameAndPassword[1]
                    ];
                }
            } catch (\Exception $e) {
                return null;
            }
        }
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $userProvider->loadUserByUsername($credentials['username']);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $credentials['password'] === $user->getPassword() && null === $user->getSalt();
    }

    public function onAuthenticationSuccess(HttpFoundation\Request $request, TokenInterface $token, $providerKey)
    {
        $request->attributes->set('user', $token->getUser());
        return null;
    }

    public function onAuthenticationFailure(HttpFoundation\Request $request, AuthenticationException $exception)
    {
        return $this->responseFactory->createAuthenticationFailureResponse($request);
    }

    public function start(HttpFoundation\Request $request, AuthenticationException $authException = null)
    {
        return $this->responseFactory->createAuthenticationFailureResponse($request);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
