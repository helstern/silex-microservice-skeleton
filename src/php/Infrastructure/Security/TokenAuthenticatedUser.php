<?php namespace Helstern\SMSkeleton\Infrastructure\Security;

use Helstern\SMSkeleton\Application;
use Symfony\Component\Security\Core\User\UserInterface;

class TokenAuthenticatedUser implements UserInterface
{
    /** @var string */
    private $clientId;

    /** @var string */
    private $token;

    /**
     * ServiceUser constructor.
     * @param string $clientId
     * @param string $rawToken
     */
    public function __construct($clientId, $rawToken)
    {
        $this->clientId = $clientId;
        $this->token = $rawToken;
    }

    public function getRoles()
    {
        return [Application\RolesEnum::API_USER];
    }

    public function getPassword()
    {
        return $this->token;
    }

    public function getSalt()
    {
        return '';
    }

    public function getUsername()
    {
        return $this->clientId;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
