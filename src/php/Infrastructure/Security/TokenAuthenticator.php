<?php namespace Helstern\SMSkeleton\Infrastructure\Security;

use Helstern\SMSkeleton\Application\SecureUserTokenIssuer;
use Helstern\SMSkeleton\HttpApi\Authenticate;
use Symfony\Component\HttpFoundation;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /** @var Authenticate\AuthenticationResponseFactory */
    private $responseFactory;

    /** @var SecureUserTokenIssuer */
    private $tokenIssuer;

    public function __construct(SecureUserTokenIssuer $tokenIssuer, Authenticate\AuthenticationResponseFactory $responseFactory)
    {
        $this->tokenIssuer = $tokenIssuer;
        $this->responseFactory = $responseFactory;
    }

    /**
     * Called on every request. Return whatever credentials you want,
     * or null to stop authentication.
     */
    public function getCredentials(HttpFoundation\Request $request)
    {
        if (!$token = $request->headers->get('AUTHORIZATION')) {
            // no token? Return null and no other methods will be called
            return null;
        }
        $tokenParts = explode(' ', $token);
        if (count($tokenParts) == 2 && $tokenParts[0] == 'Bearer') {
            try {
                $token = $this->tokenIssuer->decode($tokenParts[1]);
                return [
                    'encoded_token' => $tokenParts[1],
                    'token' => $token
                ];
            } catch (\Exception $e) {
                return null;
            }
        }
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $userId = $credentials['token']->userId;
        return new TokenAuthenticatedUser($userId, $credentials['encoded_token']);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // no credential check is needed in this case
        return true;
    }

    public function onAuthenticationSuccess(HttpFoundation\Request $request, TokenInterface $token, $providerKey)
    {
        $request->attributes->set('user', $token->getUser());
        return null;
    }

    public function onAuthenticationFailure(HttpFoundation\Request $request, AuthenticationException $exception)
    {
        return $this->responseFactory->createAuthenticationFailureResponse($request);
    }

    public function start(HttpFoundation\Request $request, AuthenticationException $authException = null)
    {
        return $this->responseFactory->createAuthenticationFailureResponse($request);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
