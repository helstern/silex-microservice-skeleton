<?php namespace Helstern\SMSkeleton\Infrastructure\Security;

use Helstern\SMSkeleton\Application\SecureUserTokenIssuer;
use Firebase\JWT\JWT;

class JWTTokenIssuer implements SecureUserTokenIssuer
{
    /** @var string */
    private $authorityName;

    /** @var string */
    private $secret;

    /** @var string */
    private $algorithm;

    /** @var int */
    private $tokenLifetimeSeconds = 604800;

    public function __construct($authorityName, $secret, $algorithm)
    {
        $this->authorityName = $authorityName;
        $this->secret = $secret;
        $this->algorithm = $algorithm;
    }

    public function issueForUser($username)
    {
        $tokenId    = base64_encode(mcrypt_create_iv(32));
        $issuedAt   = time();
        $notBefore  = $issuedAt; //Adding 10 seconds
        $expire     = $notBefore + $this->tokenLifetimeSeconds; // Adding lifetime
        $issuer = $this->authorityName; // Retrieve the server name from config file

        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'iss'  => $issuer,       // Issuer
            'nbf'  => $notBefore,        // Not before
            'exp'  => $expire,           // Expire
            'data' => [                  // Data related to the signer user
                'userId'   => $username // userid from the users table
            ]
        ];

        // Sign the JWT with the secret key
        $jwt = JWT::encode($data, $this->secret, $this->algorithm);
        return $jwt;
    }

    /**
     * @param string $encrypted
     * @return array
     */
    public function decode($encrypted)
    {
        $token = JWT::decode($encrypted, $this->secret, [$this->algorithm]);
        return $token->data;
    }
}
