<?php namespace Helstern\SMSkeleton\Infrastructure\Application;

class Filesystem
{
    private $root;

    public function __construct($root)
    {
        $this->root = $root;
    }

    public function getCacheDir()
    {
        return $this->root . '/cache';
    }

    public function getLogsDir()
    {
        return $this->root . '/logs';
    }

    public function getTempDir()
    {
        return '/tmp';
    }
}
