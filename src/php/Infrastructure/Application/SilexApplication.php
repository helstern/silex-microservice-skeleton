<?php namespace Helstern\SMSkeleton\Infrastructure\Application;

class SilexApplication extends \Silex\Application
{
    /** @var Filesystem */
    private $filesystem;

    /**
     * @param Filesystem $filesystem
     * @param array $values
     */
    public function __construct(Filesystem $filesystem, array $values = array())
    {
        parent::__construct($values);

        $this->filesystem = $filesystem;
    }

    public function getCacheDir()
    {
        return $this->filesystem->getCacheDir();
    }

    public function getLogsDir()
    {
        return $this->filesystem->getLogsDir();
    }
}
