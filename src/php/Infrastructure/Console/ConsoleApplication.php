<?php namespace Helstern\SMSkeleton\Infrastructure\Console;

use Symfony\Component\Console;
use Helstern\SMSkeleton\Infrastructure\Application;

class ConsoleApplication extends Console\Application
{
    /** @var Application\SilexApplication */
    private $silexApplication;

    public function __construct(Application\SilexApplication $app, $name, $version)
    {
        parent::__construct($name, $version);
        $this->silexApplication = $app;
    }

    public function add(Console\Command\Command $command)
    {
        parent::add($command);
        if ($command instanceof ConsoleCommand) {
            $command->setSilexApplication($this->silexApplication);
        }
    }
}
