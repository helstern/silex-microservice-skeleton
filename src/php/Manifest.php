<?php namespace Helstern\SMSkeleton;

class Manifest
{
    const IDENTIFIER = 'SMSKELETON';

    const API_BASE_PATH = '/api';

    const API_VERSION = '0.1.0';
}
