<?php namespace Helstern\SMSkeleton\Application;

class LoggableException extends Exception implements Loggable
{
    private $logLevel;

    public function __construct($message, $code, \Exception $previous, $logLevel)
    {
        parent::__construct($message, $code, $previous);
        $this->logLevel = $logLevel;
    }

    public function getSeverityLevel() : string
    {
        return $this->logLevel;
    }
}
