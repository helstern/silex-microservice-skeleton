<?php namespace Helstern\SMSkeleton\Application;

interface Loggable
{
    public function getSeverityLevel() : string;
}
