<?php namespace Helstern\SMSkeleton\Application;

interface SecureUserTokenIssuer
{
    /**
     * @param string $username
     * @return string
     */
    public function issueForUser($username);

    /**
     * @param string $encrypted
     * @return array
     */
    public function decode($encrypted);
}
