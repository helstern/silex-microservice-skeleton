SHELL=/bin/bash
LOCAL_USER_ID:=$(shell id -u $$USER)
MAKEFILE_PATH :=$(abspath $(lastword $(MAKEFILE_LIST)))
ROOT_PATH:=$(shell dirname $(MAKEFILE_PATH))
APP_ID:=$(shell jq '.extra.identifier' < $(ROOT_PATH)/composer.json | tr -d '"' | tr '[:upper:]' '[:lower:]')

app:
	cd src/docker; export LOCAL_USER_ID=$(LOCAL_USER_ID) APP_ID=$(APP_ID); docker-compose up -d

test:
	cd src/docker; \
	export LOCAL_USER_ID=$(LOCAL_USER_ID) COMPOSE_PROJECT_NAME=$(APP_ID); \
	docker-compose --file docker-compose.yml --file test.yml run --user=$(LOCAL_USER_ID) --rm --name $(APP_ID)-test test /bin/bash -c "php app/vendor/bin/phpunit"

swagger:
	cd src/docker; \
	export LOCAL_USER_ID=$(LOCAL_USER_ID) COMPOSE_PROJECT_NAME=$(APP_ID); \
	docker-compose --file docker-compose.yml --file build.yml run --user=$(LOCAL_USER_ID) --workdir=/var/www/service build /bin/bash -c "php app/vendor/bin/swagger src/php/HttpApi src/swagger --output src/swagger/api.json --bootstrap src/swagger/bootstrap.php"

codestyle:
	cd src/docker; \
	export LOCAL_USER_ID=$(LOCAL_USER_ID) COMPOSE_PROJECT_NAME=$(APP_ID); \
	docker-compose --file docker-compose.yml --file build.yml run --user=$(LOCAL_USER_ID) --workdir=/var/www/service build /bin/bash -c "php-cs-fixer fix src/php"

clean:
	rm -rf vendor
	rm -rf app/cache/*
	rm -rf app/logs/*
	rm -rf app/data/*

dev-build:
	cd src/docker; \
	export LOCAL_USER_ID=$(LOCAL_USER_ID) COMPOSE_PROJECT_NAME=$(APP_ID); \
	docker-compose --file docker-compose.yml --file build.yml build build

dev-start:
	cd src/docker; \
	export LOCAL_USER_ID=$(LOCAL_USER_ID) COMPOSE_PROJECT_NAME=$(APP_ID); \
	docker-compose rm --force maindb-mysql; docker-compose --file docker-compose.yml --file build.yml up

dev-stop:
	cd src/docker;  \
	export LOCAL_USER_ID=$(LOCAL_USER_ID) COMPOSE_PROJECT_NAME=$(APP_ID); \
	docker-compose --file docker-compose.yml --file build.yml down --remove-orphans

.PHONY: test swagger clean dev-build dev-start dev-stop
