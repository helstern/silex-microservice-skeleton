<?php namespace Helstern\SMSkeleton\HttpApi\Test;

use Helstern\SMSkeleton\Application\SecureUserTokenIssuer;
use Silex\WebTestCase;
use Symfony\Component\BrowserKit\Client;

class HelloControllerTest extends WebTestCase
{

    public function createApplication()
    {
        $config = __DIR__.'/../../../../../app/app.php';
        $configPath = realpath($config);
        $app = require $configPath;
        $app['debug'] = true;
        unset($app['exception_handler']);

        return $app;
    }

    private function getAuthorizationToken(Client $client)
    {
        /** @var SecureUserTokenIssuer $tokenIssuer */
        $tokenIssuer = $this->app[SecureUserTokenIssuer::class];
        return $tokenIssuer->issueForUser('client_1');
    }

    public function testPostHelloUnauthorizedResponse()
    {
        $client = $this->createClient();
        $crawler = $client->request(
            'POST'
            , '/api/test/hello'
            , []
            , []
            , ['CONTENT_TYPE' => 'application/json']
        );
        $this->assertEquals(401, $client->getResponse()->getStatusCode());

        $client->request('POST', '/api/test/hello', [], [], ['HTTP_AUTHORIZATION' => 'Bearer dummy-token']);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testPostHelloBadRequestResponse()
    {
        $client = $this->createClient();
        $token = $this->getAuthorizationToken($client);

        $client->request(
            'POST'
            , '/api/test/hello'
            , [], [],
            [
                'HTTP_AUTHORIZATION' => 'Bearer ' . $token
                , 'CONTENT_TYPE' => 'application/json'
            ]
            , json_encode([
                'mjolnar' => 'joe'
            ])
        );

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testPostHelloCreatedResponse()
    {
        $client = $this->createClient();
        $token = $this->getAuthorizationToken($client);

        $client->request(
            'POST'
            , '/api/test/hello'
            , [], [],
            [
                'HTTP_AUTHORIZATION' => 'Bearer ' . $token
                , 'CONTENT_TYPE' => 'application/json'
            ]
            , json_encode([
                'recipient' => 'joe'
            ])
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

}
