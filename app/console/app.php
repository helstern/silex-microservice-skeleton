#!/usr/bin/env php
<?php
require_once __DIR__.'/../vendor/autoload.php';
set_time_limit(0);

use Helstern\SMSkeleton\Infrastructure\Console\ConsoleApplication;

$app = require __DIR__.'/../console.php';

/** @var ConsoleApplication $console */
$console = $app['console'];
$console->run();
