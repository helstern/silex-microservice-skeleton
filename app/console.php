<?php
use Helstern\SMSkeleton\Configuration\ConsoleBootstrap;

$app = require __DIR__.'/app.php';
$bootstrap = new ConsoleBootstrap();
$bootstrap->bootstrap($app, empty($_ENV) ? $_SERVER : $_ENV, new \DateTime());

return $app;
