<?php

use Helstern\SMSkeleton\Configuration\Bootstrap;
use Helstern\SMSkeleton\Infrastructure\Application;

$appFileSystem = new Application\Filesystem(__DIR__);
$app = new Application\SilexApplication($appFileSystem);

$bootstrap = new Bootstrap();
$bootstrap->bootstrap($app, empty($_ENV) ? $_SERVER : $_ENV, new \DateTime());

return $app;
